CREATE TABLE tb_subject (
    id int not null auto_increment,
    code varchar(2),
    name varchar(50),
    PRIMARY KEY (id)
);

insert into tb_subject(code, name)
values('1', 'ใบเสนอราคา');

insert into tb_subject(code, name)
values('2', 'สอบถามการใช้งาน');

insert into tb_subject(code, name)
values('3', 'เงื่อนไขการรับประกัน');

insert into tb_subject(code, name)
values('4', 'อื่นๆ');
