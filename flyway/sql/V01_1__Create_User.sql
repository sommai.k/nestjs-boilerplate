CREATE TABLE user (
    id int not null auto_increment,
    code varchar(5),
    name varchar(50),
    email varchar(50),
    pwd varchar(8),
    role varchar(1),
    active varchar(1),
    PRIMARY KEY (id)
);

insert into user(code, name, email, role, active, pwd)
values('00001', 'Admin', 'a@a.com', 'A', 'Y', '1111');

insert into user(code, name, email, role, active, pwd)
values('U0001', 'User', 'u@u.com', 'U', 'Y', '2222');